//=============================================================================
// d3dUtil.h by Frank Luna (C) 2005 All Rights Reserved.
//
// Contains various utility code for DirectX applications, such as, clean up
// and debugging code.
//=============================================================================

#ifndef D3DUTIL_H
#define D3DUTIL_H

// Enable extra D3D debugging in debug builds if using the debug DirectX runtime.  
// This makes D3D objects work well in the debugger watch window, but slows down 
// performance slightly.
#if defined(DEBUG) | defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

#include <d3d9.h>
#include <d3dx9.h>
#include <dxerr9.h>
#include <sstream>

//===============================================================
// Globals for convenient access.
class D3DApp;
extern D3DApp* gd3dApp;
extern IDirect3DDevice9* gd3dDevice;

//===============================================================
// Clean up

#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }

// ========== Materials and Lights ==========
struct Material
{
	Material():ambient(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
		       diffuse(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
			   specular(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
			   specularPower(5.0f){}

	Material(const D3DXCOLOR& amb,
		     const D3DXCOLOR& dif,
			 const D3DXCOLOR& spe,
			 float pwr):ambient(amb),
			            diffuse(dif),
						specular(spe),
						specularPower(pwr){}

	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR specular;
	float specularPower;
};

struct DirectionalLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffues;
	D3DXCOLOR specular;
	D3DXVECTOR3 directionWorld;
};

struct MeshMaterial
{
	D3DXCOLOR ambient;	//Ka
	D3DXCOLOR diffuse;	//Kd
	D3DXCOLOR spec;		//Ks
	float specPower;	//Ns
};

struct PointLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	D3DXVECTOR3 posW;
	float range;
};

//===Graphic Bounding Volumes====================================
struct AABB 
{
	// Initialize to an infinitely small bounding box.
	AABB()
		: minPt(FLT_MAX, FLT_MAX, FLT_MAX),
		  maxPt(-FLT_MAX, -FLT_MAX, -FLT_MAX){}

    D3DXVECTOR3 center()const
	{
		return (minPt+maxPt)*0.5f;
	}

	D3DXVECTOR3 extent()const
	{
		return (maxPt-minPt)*0.5f;
	}

	void xform(const D3DXMATRIX& M, AABB& out)
	{
		// Convert to center/extent representation.
		D3DXVECTOR3 c = center();
		D3DXVECTOR3 e = extent();

		// Transform center in usual way.
		D3DXVec3TransformCoord(&c, &c, &M);

		// Transform extent.
		D3DXMATRIX absM;
		D3DXMatrixIdentity(&absM);
		absM(0,0) = fabsf(M(0,0)); absM(0,1) = fabsf(M(0,1)); absM(0,2) = fabsf(M(0,2));
		absM(1,0) = fabsf(M(1,0)); absM(1,1) = fabsf(M(1,1)); absM(1,2) = fabsf(M(1,2));
		absM(2,0) = fabsf(M(2,0)); absM(2,1) = fabsf(M(2,1)); absM(2,2) = fabsf(M(2,2));
		D3DXVec3TransformNormal(&e, &e, &absM);

		// Convert back to AABB representation.
		out.minPt = c - e;
		out.maxPt = c + e;
	}

	D3DXVECTOR3 minPt;
	D3DXVECTOR3 maxPt;
};

struct BoundingSphere 
{
	BoundingSphere()
		: pos(0.0f, 0.0f, 0.0f), radius(0.0f){}

	D3DXVECTOR3 pos;
	float radius;
};
//===============================================================

//===Engine Schematics===========================================
struct MeshSchematic
{
	ID3DXMesh			*mesh;
	LPDIRECT3DTEXTURE9	maps[5];
	DWORD				detailFlags;
	AABB				*BoundingBox;
	BoundingSphere		*BoundingSphere;
};
//===============================================================

//===Math Helper Functions=======================================

D3DXVECTOR3 QuatForwardVec(D3DXQUATERNION q);
D3DXVECTOR3 QuatUpVec(D3DXQUATERNION q);
D3DXVECTOR3 QuatRightVec(D3DXQUATERNION q);

//===============================================================

// Debug

#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)                                      \
	{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
		{                                              \
			DXTrace(__FILE__, __LINE__, hr, #x, TRUE); \
		}                                              \
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif 

#endif // D3DUTIL_H