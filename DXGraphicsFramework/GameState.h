#pragma once
//#include <d3dx9.h>

class Game;

class GameState{

public:
	
	virtual void InitializeState() = 0;
    virtual void UpdateScene(float dt) = 0;
    virtual void RenderScene() = 0;
    virtual void OnResetDevice() = 0;
    virtual void OnLostDevice() = 0;
    virtual void LeaveState() = 0;

	virtual ~GameState(){}
};

