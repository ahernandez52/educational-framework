#pragma once
#include "d3dApp.h"
#include <tchar.h>
#include <crtdbg.h>
#include "GfxStats.h"
#include "GameStateMachine.h"
#include "DirectInput.h"
#include "Camera.h"
#include "XBOXController.h"
//#include "SkyBox.h"


class DXMain : public D3DApp
{
	friend class WorkBench;
public:
	DXMain(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~DXMain();

	bool checkDeviceCaps();
	void onLostDevice();
	void onResetDevice();
	void updateScene(float dt);
	void drawScene();

	void startGSM();
	void startHUD();

	GameStateMachine	*getGSM();

	GfxStats			*m_GfxStats;

private:
	GameStateMachine	*GSM;

	Camera				*m_Camera;
	//SkyBox				*m_skyBox;

	bool				m_paused;
	bool				m_lighting;

	void EventXBOXController();
};