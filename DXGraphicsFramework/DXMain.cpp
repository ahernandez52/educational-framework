//=============================================================================
// HelloDirect3D.cpp by Frank Luna (C) 2005 All Rights Reserved.
//
// Demonstrates Direct3D Initialization and text output using the 
// framework code.
//=============================================================================

#include "DXMain.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	#endif

	//_CrtSetBreakAlloc(0);

	DXMain app(hInstance, "Hello Direct3D", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;

	DirectInput di(DISCL_NONEXCLUSIVE|DISCL_FOREGROUND, DISCL_NONEXCLUSIVE|DISCL_FOREGROUND);
	gDInput = &di;



	//MantisSprite msi;
	//gSprite = &msi;

	app.startGSM();
	
	//MSI->init();
	//MSI->PlaySoundA("Sounds/background.mp3");


	//MSI->PlaySoundA("Sounds/zone_nebula_dmatter.wav", true);

	//MSI->PlaySoundA("Chemical_Imbalance.wav");
	//MSI->PlaySoundA("Big_Dawg.wav");
	//MSI->PlaySoundA("Chemical_Imbalance.wav");

	//MVI->init("vid1.avi");
	//MVI->SetPlayback("vid1.avi");
	//MVI->RunPlayback();
	
	return gd3dApp->run();
}

DXMain::DXMain(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	srand(time_t(0));

	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}

	gXinput = new XBOXController(1);
	//m_xboxActive = true;
	m_GfxStats = new GfxStats();
	//m_skyBox = new SkyBox("Assets/Meshes/Skybox/space_cube_02.dds", 20000.0f);
	
	gCamera->setSpeed(800);
	gCamera->setLens(D3DXToRadian(45), (float)md3dPP.BackBufferWidth /  (float)md3dPP.BackBufferHeight, 1.0f, 50000.0f);

	m_paused = false;

    D3DLIGHT9 light;    // create the light struct   
	D3DMATERIAL9 material;    // create the material struct    
	ZeroMemory(&light, sizeof(light));    // clear out the light struct for use    
	light.Type = D3DLIGHT_DIRECTIONAL;    // make the light type 'directional light'    
	light.Diffuse = D3DXCOLOR(0.7f, 0.7f, 0.7f, 1.0f);    // set the light's color    
	light.Direction = D3DXVECTOR3(-1.0f, -0.3f, -1.0f);    
	gd3dDevice->SetLight(0, &light);    // send the light struct properties to light #0    
	gd3dDevice->LightEnable(0, TRUE);    // turn on light #0    
	ZeroMemory(&material, sizeof(D3DMATERIAL9));    // clear out the struct for use    
	material.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set diffuse color to white    
	material.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set ambient color to white    
	gd3dDevice->SetMaterial(&material);    // set the globably-used material to &material

	gd3dDevice->SetRenderState(D3DRS_LIGHTING, true);
	gd3dDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(50, 50, 50));

	gCamera->fixedDist() = 400;
	gCamera->lookAt(D3DXVECTOR3(50.0f, 100.0f, -50.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	gCamera->setInput(PC_MOUSEKEYBOARD);
	gCamera->setMode(FIXED_ROTATION);
}

DXMain::~DXMain()
{
	delete m_GfxStats;
	delete GSM;
	//delete m_HUD;
	delete gXinput;
	//delete m_skyBox;
}

bool DXMain::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

void DXMain::onLostDevice()
{
	m_GfxStats->onLostDevice();
	//m_skyBox->onLostDevice();

	//HR(gSprite->mSprite->OnLostDevice());
	GSM->OnLostDevice();
}

void DXMain::onResetDevice()
{
	m_GfxStats->onResetDevice();
	//m_skyBox->onResetDevice();
	gCamera->onResetDevice((float)md3dPP.BackBufferWidth, (float)md3dPP.BackBufferHeight);

	//HR(gSprite->mSprite->OnResetDevice());
	GSM->OnResetDevice();
	//m_HUD->OnResetDevice();

	D3DLIGHT9 light;    // create the light struct   
	D3DMATERIAL9 material;    // create the material struct    
	ZeroMemory(&light, sizeof(light));    // clear out the light struct for use    
	light.Type = D3DLIGHT_DIRECTIONAL;    // make the light type 'directional light'    
	light.Diffuse = D3DXCOLOR(0.7f, 0.7f, 0.7f, 1.0f);    // set the light's color    
	light.Direction = D3DXVECTOR3(-1.0f, -0.3f, -1.0f);    
	gd3dDevice->SetLight(0, &light);    // send the light struct properties to light #0    
	gd3dDevice->LightEnable(0, TRUE);    // turn on light #0    
	ZeroMemory(&material, sizeof(D3DMATERIAL9));    // clear out the struct for use    
	material.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set diffuse color to white    
	material.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set ambient color to white    
	gd3dDevice->SetMaterial(&material);    // set the globably-used material to &material

	gd3dDevice->SetRenderState(D3DRS_LIGHTING, true);
	gd3dDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(50, 50, 50));

	//gd3dDevice->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, 8);    // anisotropic level    
	gd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);    // minification    
	gd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);    // magnification    
	gd3dDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);    // mipmap

	//HR(gd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE));
	//HR(gd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1));

	//HR(gd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA));
	//HR(gd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA));
	
	m_lighting = true;
}

void DXMain::startGSM()
{
	GSM = new GameStateMachine();
	//GSM->SetCurrentState(new LevelEditing(this));
	GSM->InitializeState();
}


GameStateMachine* DXMain::getGSM()
{
	return GSM;
}


void DXMain::updateScene(float dt)
{
	m_GfxStats->update(dt);
	gDInput->poll();
	EventXBOXController();
	//MSI->update();
	//MSI->set3DEars(gCamera->pos(), gCamera->pos(), gCamera->look(), gCamera->up());
	gCamera->update(dt);

	if(gXinput->current[0].Start && (gXinput->previous[0].Start == false))
	{
		if(!m_paused)
		{
			m_paused = true;
			//GSM->ChangeState(new GameMenu(this));
		} else {
			m_paused = false;
			GSM->ReverToPreviousState();
		}
	}

	if(gXinput->current[0].RShoulder && (gXinput->previous[0].RShoulder == false))
		m_lighting = !m_lighting;

	
	GSM->UpdateScene(dt);	
	
}

void DXMain::drawScene()
{
	HR(gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x33333333, 1.0f, 0));

	HR(gd3dDevice->BeginScene());

	gd3dDevice->SetRenderState(D3DRS_LIGHTING, m_lighting);




	//m_skyBox->render();
	m_GfxStats->display();
	GSM->RenderScene();

	//HR(gSprite->mSprite->Begin(D3DXSPRITE_ALPHABLEND));

	//HR(gSprite->mSprite->Flush());
	//HR(gSprite->mSprite->End());

	HR(gd3dDevice->EndScene());

	HR(gd3dDevice->Present(0, 0, 0, 0));
}

void DXMain::EventXBOXController()
{
	lock.acquire();

	//if the thread is asleep don't poll
	//if(gXinput->ThreadKillState == 1)
	//{
	//	DXCI->setControllerMode(ControlModeMouse);
	//	return;
	//}

	//DXCI->setControllerMode(ControlModeXBOX);

	//check for fullscreen toggle
	if(gXinput->current[0].Select && (gXinput->previous[0].Select == false))
		enableFullScreenMode(md3dPP.Windowed);

	//send the controller structure to ControllerPoll
	//gMyGameWorld->ControllerPoll(gXinput->current[0], gXinput->previous[0]);

	gXinput->wait = false;
	

	lock.release();
}