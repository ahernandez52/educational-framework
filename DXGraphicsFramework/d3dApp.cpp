//=============================================================================
// d3dApp.h by Frank Luna (C) 2005 All Rights Reserved.
//=============================================================================

#include "d3dApp.h"
#include <Windows.h>

D3DApp* gd3dApp              = 0;
IDirect3DDevice9* gd3dDevice = 0;

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if( gd3dApp != 0 )
		return gd3dApp->msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}

D3DApp::D3DApp(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
{
	mMainWndCaption = winCaption;
	mDevType        = devType;
	mRequestedVP    = requestedVP;

	mhAppInst   = hInstance;
	mhMainWnd   = 0;
	md3dObject  = 0;
	mAppPaused  = false;
	ZeroMemory(&md3dPP, sizeof(md3dPP));

	initMainWindow();
	initDirect3D();
}

D3DApp::~D3DApp()
{
	ReleaseCOM(md3dObject);
	ReleaseCOM(gd3dDevice);
}

HINSTANCE D3DApp::getAppInst()
{
	return mhAppInst;
}

HWND D3DApp::getMainWnd()
{
	return mhMainWnd;
}

void D3DApp::initMainWindow()
{
	WNDCLASS wc;
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = MainWndProc; 
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = mhAppInst;
	wc.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	//wc.lpszMenuName  = (LPSTR)IDR_MENU1;
	wc.lpszClassName = "D3DWndClassName";

	if( !RegisterClass(&wc) )
	{
		MessageBox(0, "RegisterClass FAILED", 0, 0);
		PostQuitMessage(0);
	}

	// Default to a window with a client area rectangle of 800x600.

	RECT R = {0, 0, 800, 600};
	AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	mhMainWnd = CreateWindow("D3DWndClassName", mMainWndCaption.c_str(), 
		WS_OVERLAPPEDWINDOW, 100, 100, R.right, R.bottom, 
		0, 0, mhAppInst, 0); 

	if( !mhMainWnd )
	{
		MessageBox(0, "CreateWindow FAILED", 0, 0);
		PostQuitMessage(0);
	}

	ShowWindow(mhMainWnd, SW_SHOW);
	UpdateWindow(mhMainWnd);
}

void D3DApp::initDirect3D()
{
	md3dObject = Direct3DCreate9(D3D_SDK_VERSION);
	if( !md3dObject )
	{
		MessageBox(0, "Direct3DCreate9 FAILED", 0, 0);
		PostQuitMessage(0);
	}


	D3DDISPLAYMODE mode;
	md3dObject->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
	HR(md3dObject->CheckDeviceType(D3DADAPTER_DEFAULT, mDevType, mode.Format, mode.Format, true));
	HR(md3dObject->CheckDeviceType(D3DADAPTER_DEFAULT, mDevType, D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false));

	D3DCAPS9 caps;
	HR(md3dObject->GetDeviceCaps(D3DADAPTER_DEFAULT, mDevType, &caps));

	DWORD devBehaviorFlags = 0;
	if( caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT )
		devBehaviorFlags |= mRequestedVP;
	else
		devBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;


	if( caps.DevCaps & D3DDEVCAPS_PUREDEVICE &&
		devBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING)
		devBehaviorFlags |= D3DCREATE_PUREDEVICE;


	md3dPP.BackBufferWidth            = 0; 
	md3dPP.BackBufferHeight           = 0;
	md3dPP.BackBufferFormat           = D3DFMT_UNKNOWN;
	md3dPP.BackBufferCount            = 1;
	md3dPP.MultiSampleType            = D3DMULTISAMPLE_NONE;
	md3dPP.MultiSampleQuality         = 0;
	md3dPP.SwapEffect                 = D3DSWAPEFFECT_DISCARD; 
	md3dPP.hDeviceWindow              = mhMainWnd;
	md3dPP.Windowed                   = true;
	md3dPP.EnableAutoDepthStencil     = true; 
	md3dPP.AutoDepthStencilFormat     = D3DFMT_D24S8;
	md3dPP.Flags                      = 0;
	md3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	md3dPP.PresentationInterval       = D3DPRESENT_INTERVAL_IMMEDIATE;

	HR(md3dObject->CreateDevice(
		D3DADAPTER_DEFAULT, 
		mDevType,           
		mhMainWnd,          
		devBehaviorFlags,   
		&md3dPP,            
		&gd3dDevice));      
}

int D3DApp::run()
{
	//HACCEL hAccel = LoadAccelerators(mhAppInst, MAKEINTRESOURCE(IDR_ACCELERATOR1));

	MSG  msg;
	msg.message = WM_NULL;

	static float TE = 0.0f;

	__int64 cntsPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	float secsPerCnt = 1.0f / (float)cntsPerSec;

	__int64 prevTimeStamp = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTimeStamp);

	while(msg.message != WM_QUIT)
	{

		if(PeekMessage( &msg, 0, 0, 0, PM_REMOVE ))
		{
			//if(!TranslateAccelerator(mhMainWnd, hAccel, &msg))
			//{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			//}
		}
		else
		{	

			/*if( mAppPaused )
			{
				Sleep(20);
				continue;
			}*/

			if( !isDeviceLost() )
			{
				__int64 currTimeStamp = 0;
				QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
				float dt = (currTimeStamp - prevTimeStamp)*secsPerCnt;

				updateScene(dt);

				drawScene();

				prevTimeStamp = currTimeStamp;
			}
		}
	}
	return (int)msg.wParam;
}


LRESULT D3DApp::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{

	static bool minOrMaxed = false;

	RECT clientRect = {0, 0, 0, 0};
	switch( msg )
	{


	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE )
			mAppPaused = true;
		else
			mAppPaused = false;
		return 0;



	case WM_SIZE:
		if( gd3dDevice )
		{
			md3dPP.BackBufferWidth  = LOWORD(lParam);
			md3dPP.BackBufferHeight = HIWORD(lParam);

			if( wParam == SIZE_MINIMIZED )
			{
				mAppPaused = true;
				minOrMaxed = true;
			}
			else if( wParam == SIZE_MAXIMIZED )
			{
				mAppPaused = false;
				minOrMaxed = true;
				onLostDevice();
				HR(gd3dDevice->Reset(&md3dPP));
				onResetDevice();
			}

			else if( wParam == SIZE_RESTORED )
			{
				mAppPaused = false;


				if( minOrMaxed && md3dPP.Windowed )
				{
					onLostDevice();
					HR(gd3dDevice->Reset(&md3dPP));
					onResetDevice();
				}
				else
				{

				}
				minOrMaxed = false;
			}
		}
		return 0;



	case WM_EXITSIZEMOVE:
		GetClientRect(mhMainWnd, &clientRect);
		md3dPP.BackBufferWidth  = clientRect.right;
		md3dPP.BackBufferHeight = clientRect.bottom;

		onLostDevice();
		HR(gd3dDevice->Reset(&md3dPP));
		onResetDevice();

		return 0;


	case WM_CLOSE:
		DestroyWindow(mhMainWnd);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_KEYDOWN:
		if( wParam == VK_ESCAPE )
			enableFullScreenMode(false);
		else if( wParam == 'F' )
			enableFullScreenMode(true);
		return 0;

	//case WM_CONTEXTMENU:
	//	{
	//		
	//		RECT rc;
	//		POINT pt = {GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)};

	//		GetClientRect(mhMainWnd, &rc);

	//		ScreenToClient(mhMainWnd, &pt);

	//		if(PtInRect(&rc, pt))
	//		{
	//			ClientToScreen(mhMainWnd, &pt);
	//			//DisplayContext
	//			HMENU hmenu;			//top-level menu
	//			HMENU hmenuTrackPopup;	//shortcut menu

	//			if((hmenu = LoadMenu(mhAppInst, (LPSTR)IDR_MENU2)) == NULL)
	//				return 0;

	//			hmenuTrackPopup = GetSubMenu(hmenu, 0);

	//			TrackPopupMenu(hmenuTrackPopup, TPM_LEFTALIGN | TPM_RIGHTBUTTON,
	//						   pt.x, pt.y, 0, mhMainWnd, NULL);

	//			DestroyMenu(hmenu);
	//		}
	//		return 0;
	//	}
	
	//case WM_COMMAND:
	//	switch(LOWORD(wParam))
	//	{
	//	//===== Shortcuts ====================
	//	case ID_SAVE:
	//		WDM->Save();
	//		return 0;

	//	case ID_NEW:
	//		WDM->New();
	//		return 0;

	//	case ID_OPEN:
	//		WDM->Open();
	//		return 0;

	//	case ID_CUT:
	//		WDM->Cut();
	//		return 0;

	//	case ID_COPY:
	//		WDM->Copy();
	//		return 0;

	//	case ID_PASTE:
	//		WDM->Paste();
	//		return 0;

	//	case ID_UNDO:
	//		//NYI WDM->Undo();
	//		return 0;

	//	case ID_REDO:
	//		//NYI WDM->Redo();
	//		return 0;

	//	case ID_DOC:
	//		WDM->Documentation();
	//		return 0;

	//	case ID_ABOUT:
	//		WDM->About();
	//		return 0;

	//	//====================================

	//	//===== File =========================
	//	case ID_FILE_NEW40001:
	//		WDM->New();
	//		return 0;

	//	case ID_FILE_SAVE40002:
	//		WDM->Save();
	//		return 0;

	//	case ID_FILE_SAVEAS:
	//		WDM->SaveAs();
	//		return 0;
	//	
	//	case ID_FILE_LOAD:
	//		WDM->Open();
	//		return 0;
	//		
	//	case ID_FILE_EXPORT:
	//		WDM->LevelChange();
	//		//WDM->ExportLevel();
	//		return 0;

	//	case ID_FILE_EXIT:
	//		WDM->Unload();
	//		DestroyWindow(mhMainWnd);
	//		return 0;
	//	//====================================

	//	//===== Edit =========================
	//	case ID_EDIT_COPY40006:
	//		WDM->Copy();
	//		return 0;

	//	case ID_EDIT_CUT40007:
	//		WDM->Cut();
	//		return 0;

	//	case ID_EDIT_PASTE40008:
	//		WDM->Paste();
	//		return 0;
	//	//====================================

	//	//===== View =========================
	//	case ID_VIEW_ASSETS:
	//		//!WDM->assetsIsActive() ? WDM->ToggleAssets() : 0;	
	//		if(!WDM->assetsIsActive()) WDM->ToggleAssets();
	//		return 0;
	//		
	//	case ID_VIEW_LEVELS:
	//		//!WDM->LevelsIsActive() ? WDM->ToggleLevels() : 0;			
	//		if(!WDM->LevelsIsActive()) WDM->ToggleLevels();
	//		return 0;

	//	case ID_VIEW_NODES:
	//		//!WDM->NodesIsActive() ? WDM->ToggleNodes() : 0;
	//		//if(!WDM->NodesIsActive()) WDM->ToggleNodes();
	//		return 0;

	//	case ID_VIEW_ZONES:
	//		//!WDM->ZonesIsActive() ? WDM->ToggleZones() : 0;
	//		//if(!WDM->ZonesIsActive()) WDM->ToggleZones();
	//		return 0;

	//	case ID_VIEW_TRANSFORMS:
	//		//!WDM->TransformsIsActive() ? WDM->ToggleTransforms() : 0;
	//		if(!WDM->TransformsIsActive()) WDM->ToggleTransforms();
	//		return 0;

	//	case ID_VIEW_PROPERTIES:
	//		if(!WDM->PropertyIsActive()) WDM->ToggleProperty();
	//		return 0;
	//	//====================================

	//	//===== Tools ========================
	//	case ID_TOOLS_GUI:
	//		//function
	//		return 0;

	//	case ID_TOOLS_SHADERS:
	//		//function
	//		return 0;

	//	case ID_TOOLS_ZONES:
	//		//function
	//		return 0;

	//	case ID_TOOLS_GENERATOR:
	//		//function
	//		return 0;
	//	//====================================

	//	//===== Help =========================
	//	case ID_HELP_DOCUMENTATION:
	//		WDM->Documentation();
	//		return 0;

	//	case ID_HELP_ABOUT:
	//		WDM->About();
	//		return 0;
	//	//====================================
		//}

		//return 0;
	}

	return DefWindowProc(mhMainWnd, msg, wParam, lParam);
}

void D3DApp::enableFullScreenMode(BOOL enable)
{
	if( enable )
	{
		if( !md3dPP.Windowed ) 
			return;

		int width  = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		md3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
		md3dPP.BackBufferWidth  = width;
		md3dPP.BackBufferHeight = height;
		md3dPP.Windowed         = false;

		SetWindowLongPtr(mhMainWnd, GWL_STYLE, WS_POPUP);

		SetWindowPos(mhMainWnd, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);	
	}
	else
	{
		if( md3dPP.Windowed ) 
			return;

		RECT R = {0, 0, 800, 600};
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
		md3dPP.BackBufferFormat = D3DFMT_UNKNOWN;
		md3dPP.BackBufferWidth  = 800;
		md3dPP.BackBufferHeight = 600;
		md3dPP.Windowed         = true;

		SetWindowLongPtr(mhMainWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		SetWindowPos(mhMainWnd, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	}

	onLostDevice();
	HR(gd3dDevice->Reset(&md3dPP));
	onResetDevice();
}

bool D3DApp::isDeviceLost()
{

	HRESULT hr = gd3dDevice->TestCooperativeLevel();


	if( hr == D3DERR_DEVICELOST )
	{
		Sleep(20);
		return true;
	}

	else if( hr == D3DERR_DRIVERINTERNALERROR )
	{
		MessageBox(0, "Internal Driver Error...Exiting", 0, 0);
		PostQuitMessage(0);
		return true;
	}

	else if( hr == D3DERR_DEVICENOTRESET )
	{
		onLostDevice();
		HR(gd3dDevice->Reset(&md3dPP));
		onResetDevice();
		return false;
	}
	else
		return false;
}

BOOL CALLBACK DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
	return 0;//WDM->msgProc(hwndDlg, message, wParam, lParam); 
} 